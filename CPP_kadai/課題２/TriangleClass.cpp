/*
TriangleClass.cpp
TriangleClass クラスの各メンバ関数の定義
*/

//ヘッダをインクルード
#include<iostream>
#include"TriangleClass.h"

//代入
void TriangleClass::Input()
{
	teihen = 20.0f;
	takasa = 15.0f;
}

//面積
void TriangleClass::Calc()
{
	menseki = teihen * takasa / 2.0f;
}

//結果
void TriangleClass::Disp()
{
	std::cout << "三角形の面積=" << menseki << "\n";
}