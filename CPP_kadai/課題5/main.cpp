#include<iostream>
#include"Data.h"

//メイン関数
int main() {
	//変数iに値を代入させる
	int i;
	std::cin >> i;

	//Dateクラスのインスタンスを作る
	Data x;

	//メンバ関数を呼び出す
	x.SetValue(i); //変数iの値を渡す（アクセス関数）
	x.Disp(); //表示する
}