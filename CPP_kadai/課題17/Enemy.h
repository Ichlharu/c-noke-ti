#include "Character.h"

class Enemy:public Character
{


public:
	Enemy();
	void DispHp();
	int Attack(int i);
	void Damage(int i);
};