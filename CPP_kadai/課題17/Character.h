#pragma once
class Character
{
protected:
	int hp;
	int atk;
	int def;
public:

	int GetDef();
	bool IsDead();
};