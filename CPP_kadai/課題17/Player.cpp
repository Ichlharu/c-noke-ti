#include "Player.h"
#include <iostream>
//コンストラクタ（初期化）
Player::Player()
{
	hp = 300; //HP
	atk = 50; //攻撃力
	def = 35; //防御力
}
void Player::DispHp()
{
	std::cout << "プレイヤーＨＰ＝" << hp << "\n";
}
int  Player::Attack(int i)
{
	printf("プレイヤーの攻撃！ ");
	return atk - i / 2;
}
void  Player::Damage(int i)
{
	std::cout << "プレイヤーは" << i << "のダメージ\n";
	hp -= i;
}